from flask import Flask, url_for, request, render_template, redirect
from flask_pymongo import PyMongo
from bson.objectid import ObjectId

app = Flask(__name__)
app.config['MONGO_URI'] = 'mongodb://localhost:27017/API-REST'
mongo = PyMongo(app)

@app.route('/')
def index():
    return "Hello"

@app.route('api/v1')
def apiDoc():
    # Affiche la documentation de l'API
    # https://apidocjs.com/

@app.route('api/v1/tools')
def tools():
    # GET method : Liste toutes les veilles
    # POST method : Crée un nouvelle veille

@app.route('api/v1/tools/<id>')
def toolsId(id):
    # GET method : Donne les informations d'une veille
    # PUT method: Modifie une veille existante
    # DELETE method: Supprime une veille existante
